package com.quequiere.cityplugin.listeners;

import java.util.Optional;

import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.entity.MoveEntityEvent;
import org.spongepowered.api.event.filter.Getter;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Chunk;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.quequiere.cityplugin.CityPlugin;
import com.quequiere.cityplugin.Tools;
import com.quequiere.cityplugin.object.City;
import com.quequiere.cityplugin.object.CityChunk;
import com.quequiere.cityplugin.object.Resident;
import com.quequiere.cityplugin.visualizer.MapCityChunkVisualizer;

public class MoveListener
{

	@Listener
	public void onPlayerCorssedChunks(final MoveEntityEvent event, @Getter("getTargetEntity")	final Player p)	{
		Vector3d position1 = event.getToTransform().getLocation().getPosition();
		int x = position1.getFloorX();
		int z = position1.getFloorZ();
		//Town claims have same size as minecraft chunks, therefore we can check whenever a player crossed a chunk border
		//if a x and z coordinates are dividable by 16,
		// This check is faster than calling Vector3i#equals
		if (x == (x >> 4) << 4 || z == (z >> 4) << 4) {
			Resident r = Resident.fromPlayerId(p.getUniqueId());
			final Location<World> newLocation = event.getToTransform().getLocation();

			Location<World> previousLocation = event.getFromTransform().getLocation();
			Optional<Chunk> previousChunko = Tools.getChunk(previousLocation.getChunkPosition().getX(), previousLocation.getChunkPosition().getZ(), previousLocation.getExtent());
			Optional<Chunk> newChunko = Tools.getChunk(newLocation.getChunkPosition().getX(), newLocation.getChunkPosition().getZ(), newLocation.getExtent());

			if(!previousChunko.isPresent() ||!newChunko.isPresent() )
			{
				return;
			}

			Chunk previousChunk = previousChunko.get();
			Chunk newChunk =newChunko.get();

			City previousCity = City.getCityFromChunk(previousChunk);
			City newCity = City.getCityFromChunk(newChunk);

			if (previousCity != null && newCity == null)
			{
				CityPlugin.sendMessageWithoutPrefix("Now leaving " + previousCity.getName() + "!", TextColors.GOLD, p);
			}
			else if (newCity != null && previousCity == null || newCity != null && previousCity != null && !newCity.equals(previousCity))
			{
				CityPlugin.sendMessageWithoutPrefix("______________[ Welcome to " + newCity.getName() + " ]______________", TextColors.GOLD, p);
			}

			if (newCity != null)
			{
				CityChunk newcc = newCity.getChunck(newChunk);
				CityChunk oldcc = newCity.getChunck(previousChunk);

				if (newcc != null && newcc.getResident() != null)
				{
					if(oldcc!=null && oldcc.isOwner(newcc.getResident()))
					{
						return;
					}
					User u = Tools.getUser(newcc.getResident());
					CityPlugin.sendMessageWithoutPrefix("~~~ " + u.getName() + "'s' Land ~~~", TextColors.DARK_GREEN, p);

					if (newcc.getSellPrice() > 0)
					{
						CityPlugin.sendMessage("This land is for sale, use /cc for more info.", TextColors.AQUA, p);
					}

				}
				else if (newcc.getSellPrice() > 0)
				{
					CityPlugin.sendMessage("This land is for sale, use /cc for more info.", TextColors.AQUA, p);
				}
				else if (newcc != null && oldcc != null && newcc.getResident() == null && oldcc.getResident() != null)
				{
					CityPlugin.sendMessageWithoutPrefix("~~~ City Land ~~~", TextColors.GREEN, p);
				}
			}
		}
	}

}
