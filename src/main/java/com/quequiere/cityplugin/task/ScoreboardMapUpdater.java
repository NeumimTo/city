package com.quequiere.cityplugin.task;


import com.flowpowered.math.vector.Vector3d;
import com.quequiere.cityplugin.Tools;
import com.quequiere.cityplugin.object.Resident;
import com.quequiere.cityplugin.visualizer.MapCityChunkVisualizer;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.function.Consumer;

/**
 * Created by NeumimTo on 9.8.17.
 */
public class ScoreboardMapUpdater implements Consumer<Task> {
    private Player pl;
    private Resident r;

    public ScoreboardMapUpdater(Player pl) {
        this.pl = pl;
        r = Resident.fromPlayerId(pl.getUniqueId());
    }

    @Override
    public void accept(Task task) {

        final Location<World> newLocation = pl.getLocation();

        final Vector3d torationto = pl.getRotation();

        int toY = torationto.getFloorY() % 360;

        if (r.getCache().isDisplayMap()) {
            Direction playerDirection = Tools.getPlayerDirection(toY);
            if (!playerDirection.equals(r.getCache().getLastMapDirection()))
            {
                MapCityChunkVisualizer.updatedisplay(pl, toY, newLocation);
            }
            r.getCache().setLastMapDirection(playerDirection);
        }
    }
}
